import threading
import time
import serial
from pynput import keyboard
"""
Mobirob VGBS
yusufcc
This script was written to test sending messages within serial
from multiple threads. To test threading functionality,
keyboard inputs are used. Aim of threading is sending message
from a thread and wait for sending from another thread at same time.
last_orientation method is outdated after this test.
"""

ser = serial.Serial('/dev/ttyUSB0', 115200)

#define blow duration and time after detecting
#                         matte side of washer
duration = 8 #sec


#simple thread function just sleep and send serial messages
def thread_function(ser):
    #different mapping for messages could use.
    ser.write(bytes(b'1'))
    time.sleep(duration)
    ser.write(bytes(b'0'))

if __name__ == "__main__":
    thread_function(ser)

